This is a copy of the repo I was working on at brown, not all code is mine.
Although I should mention I wrote the vast majority of the RegistryProcessor class and child classes even tho commits don't show it.

The goal of this project was to make code that could OCR business registries in RI and eventually other states to identify and geocode past
businesses that evaded EPA regulations and likely dumped toxic waste nearby to direct cleanup efforts, it was successfully applied to RI.

# Installation

    python setup.py install

    Prereqs:

    * OpenCV 3 with python package (ironically still called `cv2`)
    * `tesseract` api modules (.so files)
    * Python packages:
        * fuzzywuzzy
        * numpy
        * scikit-learn

# For use on CCV
	make sure to run 
"export LD_LIBRARY_PATH=/gpfs/runtime/opt/gcc/4.8.2/lib64:$LD_LIBRARY_PATH" before using this on ccv (or simply make LD_LIBRARY_PATH contains that path)

# Test

        georeg_script.py --year 1979 --state RI --images test/img.png --outdir . --pre-processed
