from __future__ import print_function

import sys
import time
import numpy as np
import random
from tempfile import mkdtemp
import os.path as path
from vocab import *

#FIXME: rnn crashes on CCV saying its been "killed", this seems to only happen on ccv and may be because
# a numpy.memmap can't be passed to the gpu transparently and will cause a crash
# it's worth trying to copy the memmap to ram then pass it to the RNN (model.fit()) one piece at a time

def make_data_set(num_vocab = 3000, text_sample_ratio = 0.125):
    reg_blocks = load_data_set("./RNN training data/*-output.txt")

    # get random samples that will be easier to work with
    vocab_sample = sample_blocks(reg_blocks, 0.5)
    text_sample = sample_blocks(reg_blocks, text_sample_ratio)

    del reg_blocks

    words = get_vocab(vocab_sample, num_vocab)

    del vocab_sample

    vocab = Vocab()
    vocab.add_words(words)
    vocab.save_words("RI_vocab.tsv", "w")

    with open("reg_sample.txt", "w") as file:
        file.writelines((block + "\n" for block in text_sample))

# make_data_set()
# exit()

def make_training_examples(reg_text_fn, sentlen):
    """
    :param reg_text_fn: file name of the registry text sample file
    :param sentlen: number of words for the RNN to view before guessing next word
    :return: (sentences, next_words) unvectorized
    """

    # read in registry text sample to populate sentences and next_words with training examples
    with open(reg_text_fn, "r") as file:
        sentences = []
        next_words = []

        reg_text = file.read(1000)

        tailing_token_len = 0
        roll_over_tokens = []

        # len(reg_text) will never be less than tailing_token_len
        # so we subtract that first to find out when we aren't reading anything
        # new from the file
        while len(reg_text) - tailing_token_len > 0:
            reg_tokens = tokenize(reg_text)

            # the last token we read shall be prepended (with any trailing whitespace)
            # to the next chunk of text read, this is to avoid potentially splitting it in half
            tailing_token_len = len(reg_text) - reg_text.rindex(reg_tokens[-1])
            reg_tokens.pop()

            # upper case our tokens to remove casing details & prepend our roll over tokens
            reg_tokens = roll_over_tokens + [t.upper() for t in reg_tokens]

            # the last sentlen tokens in our token chunk are prepended
            # to our next token chunk to not miss any "sentences"
            # we'd get if all text was read and tokenized at once
            roll_over_tokens = reg_tokens[-sentlen:]

            for i in range(0, len(reg_tokens) - sentlen):
                sentences.append(reg_tokens[i: i + sentlen])
                next_words.append(reg_tokens[i + sentlen])

            # back up in file stream so we re-read the tailing token
            # in the next text chunk
            file.seek(-tailing_token_len, 1)

            # read our next chunk of text
            reg_text = file.read(1000 + tailing_token_len)

    return sentences, next_words

sentlen = 8

print('Making training sequences...')

sentences, next_words = make_training_examples("reg_sample.txt", sentlen)

print('Training sequences:', len(sentences))
print('Vectorization...')

vocab = Vocab()
vocab.load_words("RI_vocab.tsv")

# X = np.zeros((len(sentences), sentlen), dtype=np.int)
# y = np.zeros((len(sentences), len(vocab)), dtype=np.bool)

# X is the input matrix and y contains the labels (i.e. the desired output)
X = np.memmap('X_input.dat', dtype=np.uint16, mode="w+", shape=(len(sentences), sentlen))
y = np.memmap('y_output.dat', dtype=np.bool, mode="w+", shape=(len(sentences), len(vocab)))

for i, sentence in enumerate(sentences):
    for t, word in enumerate(sentence):
        X[i, t] = vocab(word)
    # may need to zero out the one-hot vector on some platforms
    # but on windows & linux it appears to be zeroed out by default
    # y[i, :] = 0
    y[i, vocab(next_words[i])] = 1

sample_indicies = random.sample(xrange(0, len(sentences), sentlen), 50)
seed_sentences = [sentences[i] for i in sample_indicies] # seed sentences are lists of tokens (not actual strings)

# free memory
del sentences
del next_words

from keras.models import Model, Sequential
from keras.layers import Input, Dense, Embedding
from keras.layers import LSTM
from keras.optimizers import RMSprop

# build the model: 2 stacked LSTM
print('Build model...')

input_layer = Input(shape=(sentlen,),dtype='uint16')
embedding = Embedding(len(vocab), 200, input_length=sentlen, dropout = 0.2)(input_layer)
lstm = LSTM(128)(embedding)
output_layer = Dense(len(vocab), activation = 'softmax')(lstm)

model = Model(input=input_layer,output=output_layer)

optimizer = RMSprop(lr=0.02)
model.compile(loss='categorical_crossentropy', optimizer=optimizer)

def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

total_epochs = 120
epochs_per_itr = 5

assert(total_epochs % epochs_per_itr == 0)

# train the model, output generated text after each iteration
for iteration in range(1, total_epochs / epochs_per_itr + 1):
    print()
    print('-' * 50)
    print('Iteration', iteration)
    model.fit(X, y, batch_size=128, nb_epoch=epochs_per_itr, validation_split=0.2)

    sentence = random.choice(seed_sentences)

    for diversity in [0.6, 0.8, 1.0, 1.2]:
        print()
        print('----- diversity:', diversity)

        generated = ' '.join(sentence)
        print('----- Generating with seed: "' + ' '.join(sentence) + '"')
        sys.stdout.write(generated)

        for i in range(50):
            x = np.zeros((1, sentlen))
            for t, word in enumerate(sentence):
                x[0, t] = vocab(word)

            preds = model.predict(x, verbose=0)[0]
            next_index = sample(preds, diversity)
            next_word = vocab(next_index)

            generated += " " + next_word
            sentence = sentence[1:] + [" " + next_word]

            sys.stdout.write(" " + next_word)
            sys.stdout.flush()
print()
