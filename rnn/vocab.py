import numpy as np
import nltk
import random
import csv
import re
import numbers

"""
This file contains the vocab class for vectorization of text and several helper functions
like tokenize() load_data_set() get_vocab() and sample_blocks()
"""


def tokenize(text, allow_numbers = True, use_upper = False):
    """
    tokenizer made specifically for text going to the RNN,
    punctuation and digits attatched to word tokens are separated and treated as unique tokens
    other functionality is toggleable
    :param text: text to tokenize
    :param allow_numbers: if False numeric tokens are filtered out
    :param use_upper: if True all returned tokens are upper cased
    :return: a list of the resulting tokens
    """

    # remove all would-be tokens that are a single '\w' character
    text = re.sub(r"\s\w\s", " ", text)

    # take all characters that are likely punctuation or chunks of numbers and separate them from adjacent tokens (they need to be represented as their own)
    text = re.sub(r"([^\w\s'<>]| *\d+ *)", r" \1 ", text)
    tokens = re.split(r"\s+", text)

    if allow_numbers:
        condition = lambda t: len(t) > 0 # remove empty tokens that always find their way in
    else:
        condition = lambda t: len(t) > 0 and not t.isdigit()

    if use_upper:
        operation = lambda t: t.upper()
    else:
        operation = lambda t: t

    tokens = [operation(t) for t in tokens if condition(t)]

    return tokens


class Vocab(object):
    def __init__(self):
        self.next_id = 0

        self._num_unkown = 0
        self._num_total = 0

        self.id_to_word = {}
        self.word_to_id = {}

        # indicies of special tokens
        self.UNKOWN_TOKEN_I = 0
        self.NUM_TOKEN_I = 1

        self.add_words(["<UNKNOWN_TOKEN>", "<NUM_TOKEN>"])

    @property
    def percent_unknown(self):
        return self._num_unkown * 1.0 / self._num_total * 100.0

    def reset_percent_unknown(self):
        self._num_unkown = 0
        self._num_total = 0

    def add_words(self, words):
        for word in words:
            if word.isdigit():
               continue # <NUM_TOKEN> is already built in

            if word not in self.word_to_id:
                self.id_to_word[self.next_id] = word
                self.word_to_id[word] = self.next_id

                self.next_id += 1

    def save_words(self, file_name, writemode = 'w'):
        with open(file_name, writemode) as file:
            writer = csv.writer(file, delimiter="\t")

            for word, id in self.word_to_id.iteritems():
                writer.writerow([word, id])

    def load_words(self, file_name):
        self.word_to_id = {}
        self.id_to_word = {}

        self.next_id = 0

        self.add_words(["<UNKNOWN_TOKEN>", "<NUM_TOKEN>"])

        with open(file_name, 'r') as file:
            reader = csv.reader(file, delimiter="\t")

            for word, id in reader:
                id = int(id)
                self.word_to_id[word] = id
                self.id_to_word[id] = word

                if id >= self.next_id:
                    self.next_id = id + 1

    def __call__(self, arg):
        """
        Convert from numerical representation to words
        and vice-versa.
        """

        if isinstance(arg, numbers.Integral):
            return self.id_to_word.get(arg, self.id_to_word[self.UNKOWN_TOKEN_I])
        else:
            assert(isinstance(arg, basestring))
            if arg.isdigit():
                index = self.NUM_TOKEN_I
            else:
                index = self.word_to_id.get(arg, self.UNKOWN_TOKEN_I)
                if index == self.UNKOWN_TOKEN_I:
                    self._num_unkown += 1
                else:
                    self._num_total += 1
            return index

    def __len__(self):
        return len(self.id_to_word)

import fnmatch
import os

def load_data_set(fn_pattern):
    """
    load a data set made from georeg with --text-dump-mode option enabled
    :param fn_pattern: linux-style file name pattern to match with data set files
    :return: a list of strings that represent the registry block for one business
    """
    dir_path, fn_pattern = os.path.split(fn_pattern)
    dir_list = os.listdir(dir_path)

    reg_blocks = []

    for item in dir_list:
        if fnmatch.fnmatch(item, fn_pattern):
            with open(os.path.join(dir_path, item), "r") as file:
                in_block = False
                reg_block = ""

                for line in file:
                    line = line.strip()

                    if in_block:
                        if line == "<BUS_END>":
                            in_block = False
                            reg_blocks.append(reg_block)
                        else:
                            reg_block += line
                    elif line == "<BUS_START>":
                        in_block = True
                        reg_block = ""

    return reg_blocks

random.seed()

def sample_blocks(reg_blocks, percent = 0.25):
    num_chunks = 1000 if len(reg_blocks) >= 1000 else len(reg_blocks)
    blocks_per_chunk = len(reg_blocks) / num_chunks

    chunks_used = random.sample(range(num_chunks), int(num_chunks * percent))
    block_sample = []

    for i in chunks_used:
        if i == num_chunks - 1:
            block_chunk = reg_blocks[i * blocks_per_chunk:]
        else:
            block_chunk = reg_blocks[i * blocks_per_chunk:(i + 1) * blocks_per_chunk]

        block_sample += block_chunk

    return block_sample

def get_vocab(reg_blocks, num_vocab = 1000):
    # we need to take a sample from
    num_chunks = 1000 if len(reg_blocks) >= 1000 else len(reg_blocks)
    blocks_per_chunk = len(reg_blocks) / num_chunks

    tokens = []
    for i in xrange(num_chunks):
        if i == num_chunks - 1:
            block_chunk = reg_blocks[i * blocks_per_chunk:]
        else:
            block_chunk = reg_blocks[i * blocks_per_chunk:(i + 1) * blocks_per_chunk]
        tokens += tokenize(" ".join(block_chunk), allow_numbers=False, use_upper=True) # for now we are upper casing all registry text

    most_common = nltk.FreqDist(tokens).most_common(num_vocab)

    return set([token for token, _ in most_common])